package pom.steps;

import co.com.sofka.automation.controller.BCLogin;
import co.com.sofka.automation.controller.DriverController;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class Autenticacion {

    WebDriver driver;

    @Before
    public void setUp() {
        driver = DriverController.getDriver();
    }

    @Dado("^un usuario en la pagina inicial de souce demo$")
    public void unUsuarioEnLaPaginaInicialDeSouceDemo() {
        BCLogin.startApp(driver, "https://www.saucedemo.com/");
    }

    @Cuando("^el usuario ingresa un \"([^\"]*)\" y \"([^\"]*)\" correctos$")
    public void elUsuarioIngresaUnYCorrectos(String user, String password) {
        BCLogin.loginUser(driver, user, password);
    }

    @Entonces("^se autentica en el sitio correctamente$")
    public void seAutenticaEnElSitioCorrectamente() {
        Assert.assertEquals(BCLogin.getTitleHome(driver), "PRODUCTS");
    }

    @After
    public void down() {
        driver.close();
    }
}
