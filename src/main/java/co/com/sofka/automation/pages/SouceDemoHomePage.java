package co.com.sofka.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SouceDemoHomePage {

    @FindBy(how = How.XPATH, using = "//*[@class='title']")
    private WebElement homeTitle;

    public SouceDemoHomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getHomeTitle() {
        return homeTitle;
    }
}
