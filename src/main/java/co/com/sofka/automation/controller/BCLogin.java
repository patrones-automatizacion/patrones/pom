package co.com.sofka.automation.controller;

import co.com.sofka.automation.pages.SouceDemoHomePage;
import co.com.sofka.automation.pages.SouceDemoLoginPage;
import org.openqa.selenium.WebDriver;

public class BCLogin {

    public static void startApp(WebDriver driver, String url) {
        driver.get(url);
    }

    public static void loginUser(WebDriver driver, String user, String password) {
        SouceDemoLoginPage souceDemoLoginPage = new SouceDemoLoginPage(driver);
        souceDemoLoginPage.getInputUser().sendKeys(user);
        souceDemoLoginPage.getInputPassword().sendKeys(password);
        souceDemoLoginPage.getLoginButton().click();
    }

    public static String getTitleHome(WebDriver driver) {
        SouceDemoHomePage homePage = new SouceDemoHomePage(driver);
        return homePage.getHomeTitle().getText();
    }
}
